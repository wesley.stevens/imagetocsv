# USAGE
# python localize_text_tesseract.py --image apple_support.png
# python localize_text_tesseract.py --image apple_support.png --min-conf 50

# import the necessary packages
from pytesseract import Output
import pytesseract
import argparse
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to input image to be OCR'd")
ap.add_argument("-c", "--min-conf", type=int, default=0,
	help="mininum confidence value to filter weak text detection")
ap.add_argument("-rx", "--roi-x", type=int, default=-1,
	help="x value of region of interest")
ap.add_argument("-ry", "--roi-y", type=int, default=-1,
	help="y value of region of interest")
ap.add_argument("-rw", "--roi-width", type=int, default=-1,
	help="width value of region of interest")
ap.add_argument("-rh", "--roi-height", type=int, default=-1,
	help="height value of region of interest")
ap.add_argument("-cols", "--num-data-columns", type=int, default=1,
	help="number of data columns")
args = vars(ap.parse_args())

#Load ROI coordinates
roi_x = args["roi_x"]
roi_y = args["roi_y"]
roi_width = args["roi_width"]
roi_height = args["roi_height"]
roi_x2 = roi_x + roi_width
roi_y2 = roi_y + roi_height
#num_data_columns = args["num_data_columns"]


# load the input image, convert it from BGR to RGB channel ordering,
# and use Tesseract to localize each area of text in the input image
image = cv2.imread(args["image"])
if ((roi_x == -1) or (roi_y == -1) or (roi_width == -1) or (roi_height == -1)):
	print("Invalid x, y, width or height parameter, using full image")
	image_roi = image
	roi_x = 0
	roi_y = 0
	roi_height, roi_width, colour = image.shape
else:
	image_roi = image[roi_y:roi_y2, roi_x:roi_x2]
rgb = cv2.cvtColor(image_roi, cv2.COLOR_BGR2RGB)

results = pytesseract.image_to_data(rgb, output_type=Output.DICT)

#Initialize dict of word data
data_dict = {}

#Create a data loop over each of the individual text localizations
for i in range(0, len(results["text"])):
	# extract the bounding box coordinates of the text region from
	# the current result
	x = results["left"][i]
	y = results["top"][i]
	w = results["width"][i]
	h = results["height"][i]

	# extract the OCR text itself along with the confidence of the
	# text localization
	text = results["text"][i]
	conf = int(results["conf"][i])

	# filter out weak confidence text localizations
	if conf > args["min_conf"]:
		# display the confidence and text to our terminal
		print("Confidence: {}".format(conf))
		print("Text: {}".format(text))
		print("")

		#Check from the vertical center of a line
		v_center = int((y + y + h)/2)

		#Amalgamate lines that are within +/- 5 pixels
		for i in range(v_center-10, v_center+10):
			if i in data_dict:
				#Amalgamate text boxes that are close together
				for j in range(x-10, x+w+5):
					if j in data_dict[i]:
						#Strip end comma and add a space
						data_dict[i][j] = data_dict[i][j][:-1] + " " + text + " "
						#Add the new text width to this dict
						x_diff = x - j
						#New width equals current width (j) + difference between two text boxes (x_diff) + the width of current text box (w)
						new_width = j + x_diff + w
						#Change the key to reflect the new width
						data_dict[i][new_width] = data_dict[i].pop(j)
						break
				else:
					#Todo make word seperator an option when running command
					data_dict[i][x+w] = text + " "			
				break
		else:
			data_dict[v_center] = {} 
			data_dict[v_center][x+w] = text + " "

		# strip out non-ASCII text so we can draw the text on the image
		# using OpenCV, then draw a bounding box around the text along
		# with the text itself
		text = "".join([c if ord(c) < 128 else "" for c in text]).strip()
		cv2.rectangle(image, (x + roi_x, y + roi_y), (x + w + roi_x, y + h + roi_y), (0, 255, 0), 2)

with open("output.csv", "w") as output:
	for key in data_dict:
		output_line = ""
		for key2 in data_dict[key]:
			#Append data by row seperated by commas
			value = data_dict[key][key2]
			print(value)
			output_line = output_line + value 
		#Replace last comma with a newline character
		output_line = output_line[:-1] + "\n"
		output.write(output_line)

# show the output image
cv2.imshow("Image", image)
cv2.waitKey(0)