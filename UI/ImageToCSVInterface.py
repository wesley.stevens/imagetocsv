import PySimpleGUI as sg
import os.path
from PIL import Image
from tkinter import PhotoImage

sg.theme('DarkAmber')   # Add a touch of color

canvas_click_state = "off"
roi_x = 0
roi_y = 0
roi_width = 0
roi_height = 0

def on_canvas_click(event):
    global canvas_click_state
    global roi_x
    global roi_y
    global roi_width
    global roi_height
    
    if canvas_click_state == "off":
        roi_x = event.x
        roi_y = event.y
        canvas_click_state = "first_click"
        canvas = event.widget
        #Draw marker indicating selection
        canvas.create_line(roi_x, roi_y, roi_x+10, roi_y,fill='blue')
        canvas.create_line(roi_x, roi_y, roi_x, roi_y+10,fill='blue')
        window.read()
    elif canvas_click_state == "first_click":
        roi_width = event.x - roi_x
        roi_height = event.y - roi_y
        canvas_click_state = "roi_selected"
        canvas = event.widget
        #Draw a box around the selection
        canvas.create_polygon(roi_x, roi_y, roi_x+roi_width,roi_y,roi_x+roi_width,roi_y+roi_height,roi_x,roi_y+roi_height,roi_x,roi_y,fill='',outline='blue')
        canvas.unbind('<Button-1>')

# First the window layout in 2 columns
file_list_column = [
    [
        sg.Text("Image Folder"),
        sg.In(size=(25, 1), enable_events=True, key="-FOLDER-"),
        sg.FolderBrowse(),
    ],
    [
        sg.Listbox(
            values=[], enable_events=True, size=(40, 20), key="-FILE LIST-"
        )
    ],
    [
        sg.Text("Template Options")
    ],
    [
        sg.Text("ROI X = ")
    ],
    [
        sg.Text("ROI Y = ")
    ],
    [
        sg.Text("ROI WIDTH = ")
    ],
    [
        sg.Text("ROI HEIGHT = ")
    ],
    [
        sg.Text("Template Name:"),
        sg.InputText('Template 1')
    ],
    [
        sg.Button('Save Template', key='-SAVE BUTTON-')
    ],
]

# For now will only show the name of the file that was chosen
image_viewer_column = [
    [sg.Text("Choose an image from list on left:")],
    [sg.Text(size=(40, 1), key="-TOUT-")],
    [sg.Canvas(size=(690, 533), key='-CANVAS-')],
]

# ----- Full layout -----
layout = [
    [
        sg.Column(file_list_column),
        sg.VSeperator(),
        sg.Column(image_viewer_column),
    ]
]

# Create the Window
window = sg.Window('ImgToCSV', layout).Finalize()

# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
        break
    
    print(event)

    # Folder name was filled in, make a list of files in the folder
    if event == "-FOLDER-":
        folder = values["-FOLDER-"]
        try:
            # Get list of files in folder
            file_list = os.listdir(folder)
        except:
            file_list = []

        fnames = [
            f
            for f in file_list
            if os.path.isfile(os.path.join(folder, f))
        ]
        window["-FILE LIST-"].update(fnames)
    elif event == "-FILE LIST-":  # A file was chosen from the listbox
        try:
            filename = os.path.join(
                values["-FOLDER-"], values["-FILE LIST-"][0]
            )
            
            canvas = window["-CANVAS-"].TKCanvas
            canvas.bind('<Button-1>', on_canvas_click)
            photo_image = PhotoImage(file=filename)
            image = canvas.create_image(690, 533, anchor='se', image=photo_image)

            window["-TOUT-"].update(filename)
            
        except Exception as e:
            print("Opening Failed")
            print(e)
            pass
    elif event == "-SAVE BUTTON-":
        try:
            if canvas_click_state != "roi_selected":
                err = ValueError()
                err.strerror = "No ROI selected"
                raise err

            #Get template name
            template_name = values[0]
            template_path = "Templates/"+template_name
            
            #Check to see if template folder exists
            if os.path.exists(template_path):
                os.rmdir(template_path)

            #Create template folder and settings file
            os.mkdir(template_path)
            with open(template_path+"/template_settings.txt", "x") as output:
                output.write("roi_x="+str(roi_x)+"\n")
                output.write("roi_y="+str(roi_y)+"\n")
                output.write("roi_width="+str(roi_width)+"\n")
                output.write("roi_height="+str(roi_height))
            
            print(template_path)
                

        except Exception as e:
            print("Save Failed")
            print(e)

window.close()